"""
Used to investigate the distribution of resiuduals of a regression for a range of frad values
"""
import ipywidgets as widgets
from ipywidgets import interact
from matplotlib import pyplot as plt
from matplotlib import cm
import numpy as np
import scipy as sp
from scipy.optimize import least_squares as least_squares
import itertools
import os
cwd = os.getcwd()
os.chdir(cwd)
from importlib import reload
import project_autumn_scripts
reload(project_autumn_scripts)
from project_autumn_scripts.NLLS_funcs import func_regr
from project_autumn_scripts.NLLS_funcs import nlls_gen

def plot_frad_qty(ax, frad, qty, ab, lab1, lab2, xlab, ylab = '$f_{rad} [-]$',setline = False, **kwargs):
    for key,value in kwargs.items():
        globals()[str(key)] = value
    ax.scatter(qty,frad[0],label=lab1+'$, I_{cc} [\\mathrm{kA}] $ '+labcur[curr], marker = ms[curr], alpha=0.7)
    ax.scatter(qty,frad[1],label=lab2+'$, I_{cc} [\\mathrm{kA}] $ '+labcur[curr], marker = ms[curr], alpha=0.7)
    if setline==True:
        ax.axhline(y=1., color='k', linestyle='-')
    ax.set_xlabel(xlab)
    ax.set_ylabel(ylab)
    ax.set_ylim(ab)

def nassim_plots(**kwargs):
    """
    Plot the observable of interest (frad, Prad) against controllable parameters for different coil currents.
    Args: Pradh, Pradv, Pheat, fradh, fradv, ne, Zeff, i_nt\\in
    Out: Relevant plots for frad analysis
    """
    # set name of keys as variable names = value
    for key,value in kwargs.items():
        globals()[str(key)] = value
        
    print("\n ----------- " + bconfig + " -------------------\n\n")

    
    plt.suptitle('Magnetic configuration: ' + str(bconfig),fontsize=16)

    
    plot_frad_qty(ax = axs[0,0],frad = [fradhcor, fradvcor], qty = ne, ab = [0.,1.5], lab1 = 'HB$C_{corr}$', 
                  lab2 = 'VB$C_{corr}$', xlab = '$n_e [10^{19} m^{-3}]$',setline=True, curr=curr,labcur = labcur, ms = ms)
    plot_frad_qty(ax = axs[0,1],frad = [fradhcor, fradvcor], qty = zeff, ab = [0.,1.5], lab1 = 'HB$C_{corr}$', 
                  lab2 = 'VB$C_{corr}$', xlab = '$Z_{eff}$ [-]',setline=True, curr=curr,labcur = labcur, ms = ms)
    
    ylab_prad = '$P_{rad} [\mathrm{MW}]$'
    plot_frad_qty(ax = axs[1,0],frad = [Pradh, Pradv], qty = ne, ab = [0.,6.], lab1 = 'HBC', 
                  lab2 = 'VBC', xlab = '$n_e [10^{19} m^{-3}]$',ylab = ylab_prad, curr=curr,labcur = labcur, ms = ms)
    plot_frad_qty(ax = axs[1,1],frad = [Pradh, Pradv], qty = zeff, ab = [0.,6.], lab1 = 'HBC', 
                  lab2 = 'VBC', xlab = '$Z_{eff}$ [-]',ylab = ylab_prad, curr=curr,labcur = labcur, ms = ms)
   
    fig.tight_layout() # ax labels does not overlap anymore
    for i in range(len(axs)):
        for j in range(len(axs[1])):
            axs[i,j].legend()

            
            
def plot_residuals(nlls, datadict, inds, ax,y=None, axs = None):
    # sample correlation coeffienct
    model = func_regr(beta=nlls.x,**datadict)
    # regression and explained sum of squares
    ssreg = np.sum((model - np.mean(model))**2)
    sstot = np.sum((y - np.mean(y))**2)
    r_corr = ssreg/sstot
    print(ssreg, sstot, r_corr)
    ax.set_title("Residuals with $r^2 = %.3f$ for \n$\\mathbf{\\beta} = ["%r_corr +str('%.2f,'*len(nlls.x))%tuple(nlls.x) +  "]$")
    ax.plot(model,  np.zeros(model.shape), '--r')
    if len(model[inds]) == len(nlls.fun):
        ax.scatter(model[inds], nlls.fun, marker = '.', label = 'Residuals')
    ax.set_ylabel("Residuals")
    ax.set_xlabel("Fitted value")
    ax.legend(loc='upper left')
    # plotting inside the residual plot
    axins = ax.inset_axes([0.7, .7, .3, .3])
    axins.hist(nlls.fun, label='Res distribution', bins = 100)
    axins.legend(loc='best')
    
def plot_reg(y, inds, params, title, label, juice, scat_col, shot_highlight = None, berr = False, str_qty = 'frad',savefig = True, fig = None, ax = None, axs = None, xlim = None, ylim = None, collim = None, plot_folder = '/home/IPP-HGW/joag/Project_Autumn/Plots/',**kwargs):
    """
    Main plotting function for regression done with NLLS.
    """
    labels = {'pheat': 'P_{Heat} $\n $ [\\mathrm{MW}]', 'nbar_e':'\\bar{n}_e $\n $ [\mathrm{m^{-3}}]', 'Zeff':'Z_{eff} $\n $ [-]', 'cis':'I_{C^{2+}} $\n $ [\\mathrm{pixel/ms}]', 'cis_intensity_aeq21_entireImage': 'I_{C^{2+}} $\n $ [\mathrm{pixel/ms}]', 't_in_shot': 't $\n $ [\\mathrm{s}]', 'png':'p_{neutral} $\n $ [\\mathrm{mBar}]', 'postboronization':'Post-boronisation'}
    
    if axs is not None:
        ax = axs[0]
        ax_res = axs[1]
    
    tuple_label = [params.x[0]]
    for i in range(len(kwargs)):
        split = labels[list(kwargs.keys())[i]].split(' $\n $ ')
        tuple_label += [ split[0] ] + [ params.x[i+1] ] + [split[1]]
    label = '$f = %.2f'%tuple_label[0] +  str('%s^{%.2f}%s'*len(kwargs))%(tuple(tuple_label[1:]))+'$'
    y_model = func_regr(beta=params.x, **kwargs)
    if not fig:
        fig, ax = plt.subplots(1,1, figsize=(10,10))
    ax.set_title(title + " $\\mathrm{SSE} = %.3f, \\mathrm{MSE} = %.3f$"%(np.sum(params.fun**2), np.mean(params.fun**2)) )
    cbar = {}
    if berr:
        ax.errorbar(y[inds],y[inds],yerr=(params.fun**2)**(1/2), label='$\\sqrt{(f-%s_{%s,exp})^{2}}$'%(str_qty[:1], str_qty[1:]), alpha = 0.1, ecolor='grey')    
    if  juice is not None:
        scat = ax.scatter(y_model[inds], y[inds], label=label, c=juice[scat_col][inds].to_numpy(), cmap='jet', s=15, alpha=1)
        if shot_highlight is not None:
            viridis = cm.get_cmap('viridis', len(shot_highlight))
            cmap_edgecols = viridis(np.linspace(0,1,len(shot_highlight)))
            i = 0
            for shot in shot_highlight:
                inds_shots = inds & juice['shot'][inds].isin([shot])
                scat_high = ax.scatter(y_model[inds_shots], y[inds_shots], label=shot, c=juice[scat_col][inds_shots].to_numpy(), cmap='jet', s=50, alpha=1, edgecolor = cmap_edgecols[i], linewidths=2, marker='^')
                i +=1
    else:
        scat = ax.scatter(y_model[inds], y[inds], label=label, c=kwargs[scat_col][inds].to_numpy(), cmap='jet', s=15, alpha=1)
        if shot_highlight is not None:
            for shot in shot_highlight:
                inds_shots = inds & juice['shot'][inds].isin([shot])
                scat_high = ax.scatter(y_model[inds_shots], y[inds_shots], label = shot, c=kwargs[scat_col][inds_shots].to_numpy(), cmap='jet', s=100, alpha=1, edgecolor = 'k', marker='^')
    ax.plot(y[inds], y[inds], 'g--', label='$f(x)=x$')
    ax.plot(y[inds], (1-0.25)*y[inds], 'k--', label='$f(x)=0.75x$')
    ax.plot(y[inds], (1+0.25)*y[inds], 'k--', label='$f(x)=1.25x$')
    ax.set_ylabel("$%s_{%s, exp}$"%(str_qty[:1], str_qty[1:4]))
    ax.set_xlabel("$f_{rad,model}$")
    if xlim: ax.set_xlim(xlim);
    if ylim: ax.set_xlim(ylim);
    if collim:
        sm = plt.cm.ScalarMappable(cmap="jet", norm=plt.Normalize(vmin=collim[0], vmax=collim[1]))
        sm.set_array([])
        cbar = plt.colorbar(ax=ax, mappable=sm)
        cbar.ax.set_xlabel('$'+labels[scat_col]+'$')
    else:
        cbar = plt.colorbar(scat, ax = ax)
        cbar.ax.set_xlabel('$'+labels[scat_col]+'$')
    ax.legend(loc='best')
    fig.tight_layout()
    if axs is not None:
        #print(params)
        plot_residuals(nlls=params, datadict=kwargs, inds=inds, ax=ax_res, y =y)
    if savefig:
        fig.savefig(plot_folder +str_qty[:4] + '_cbar_%s'%(scat_col)+'_nlls_model_corr_w'+'_%s'*len(kwargs)%(tuple([key for key,value in kwargs.items()])) + '_%.3f'*len(params.x)%tuple(params.x)+'.png',dpi=120)
    
        
def interactive_hist_residuals(y, i_y, nlls_obj, description_slider):
    """
    Plots the residuals for a range of datapoints
    Args:
     - y - data
     - i_y - the indices of the data you want to plot
     - nlls_obj - the return of the non-linear least square function from scipy
     - description_slider - name slider
    Out:
     - plot of distribution of residuals within a range of y values
    """

    pradrange_slider = widgets.FloatRangeSlider(min=min(y[interact].to_numpy()),
                                                max=max(y[interact].to_numpy()),
                                                step=0.1,
                                                description=description_slider,
                                                orientation='horizontal',
                                                readout=True,
                                                readout_format='.1f',
                                               )

    fig = plt.figure()
    def histplot_interactive(range_slider):
        inds = y[i_y] < range_slider[1]
        inds &= y[i_y]> range_slider[0]
        plt.hist(nlls_obj.fun[inds], bins=50)
        plt.show()

    interact(histplot_interactive, range_slider = pradrange_slider)
    

def plot_loss_funcs(data_power_analysis, config, bounds = (-2,2), fscale = 1.,lfuncs = ['linear', 'soft_l1', 'huber', 'cauchy', 'arctan'], plot_folder = '/home/IPP-HGW/joag/Project_Autumn/Plots/'):
    """
    Plots the regression results for every loss function to compare the effect of the outliers.
    """
    for rho in lfuncs:
        Pradh, Pradv,Pheat, fradh, fradv,  ne, zeff, inds = data_power_analysis # power_analysis(bconfig = config[1])
        #by guessing values to pinpoint the neighbourhood
        psinit = [1,1, 2.,1.95]
        qtis0 = {'pheat':Pheat, 'nbar_e':ne, 'Zeff':zeff}

        nlls_P_rad0, idxnnan_prad0 = nlls_gen(func_regr = func_regr, x0 = psinit, bounds = bounds, rho = rho, fscale = fscale, y=Pradh, **qtis0)
        fig, axs = plt.subplots(3,1,figsize=(10,10), squeeze=False)
        cl = None
        for i,j in itertools.product(*[range(x) for x in axs.shape]):
            if np.array(list(qtis0.keys())).reshape(axs.shape)[i,j] == 'Zeff':
                cl = [0,3]
            plot_reg(y = Pradh, inds = idxnnan_prad0, params = nlls_P_rad0, title = "NLLS for config " + config + ", $\\bar{e} = %.3f$"%np.mean(np.abs(nlls_P_rad0.fun)),
                     label='$f = %.2f P_{ECRH}^{%.2f}\\bar{n}_e^{%.2f}Z_{eff}^{%.2f}$'%(tuple(nlls_P_rad0.x)), juice = None, str_qty = 'Prad', 
                     scat_col = np.array(list(qtis0.keys())).reshape(axs.shape)[i,j], fig=fig, ax= axs[i,j],
                     collim=cl, plot_folder = plot_folder, **qtis0)
    


def plot_cbar_interact_bounds(ix,iy,ic):
    """
    Interactive plot where you can choose range of each axis and colorbar.
    """
    plt.figure(figsize=(10,10))
    def f_interact(ix,iy,ic):
        fig, axs = plt.subplots(3,1,figsize=(10,10), squeeze=False)
        cl = None
        for i,j in itertools.product(*[range(x) for x in axs.shape]):
            if np.array(list(qtis0.keys())).reshape(axs.shape)[i,j] == 'Zeff':
                cl = [0,3]
            plot_reg(y = Pradh, inds = idxnnan_prad0, params = nlls_P_rad0, title = "NLLS for config " + config[1],
                     label='$f = %.2f P_{ECRH}^{%.2f}\\bar{n}_e^{%.2f}Z_{eff}^{%.2f}$'%(tuple(nlls_P_rad0.x)), juice = None, str_qty = 'Prad', 
                     scat_col = np.array(list(qtis0.keys())).reshape(axs.shape)[i,j], fig=fig, ax= axs[i,j],xlim = ix, ylim=iy,
                     collim=ic,**qtis0)
    interact(f_interact, ix= ix,iy= iy,ic= ic)
    ix = widgets.FloatRangeSlider(min = 0, max = 10, value = [1,1+1], readout_format='.3f')
    iy = widgets.FloatRangeSlider(min = 0, max = 10, value = [1,1+1], readout_format='.3f')
    ic = widgets.FloatRangeSlider(min = 0, max = 10, value = [1,1+1], readout_format='.3f')

def plot_shot_cbar_interact(x, y, observables, nlls_obj, idxnnan, shotlist = None):
    """
    Plots one discharge selected by the slider.
    """
    plt.figure(figsize=(10,10))
    if shotlist:
        ishot = widgets.IntSlider(min = 0, max = len(shotlist)-1, value = 0)
    else:
        ishot = widgets.IntSlider(min = 0, max = x['shot'].unique().shape[0]-1, value = 0)
    
    def f_interact(ishot):
        """
        Args:
        - y: the dataset to be modelled by regression
        - observables: dict of observables used in regression
        - ishot - the inded of the shotlist
        - nlls_obj - the object from the NLLS scipy routine
        - idxnnnan - the indices of all values not NaN
        Out:
        - plots interactive plots for one discharge at a time
        """
        if shotlist:
            inds = x['shot'].isin([shotlist])
        else:
            inds = x['shot'].isin([x['shot'].unique()[ishot]])
        if sum(~y[idxnnan&inds].isin([np.nan])) == 0:
            return "Empty dataset"
        fig, axs = plt.subplots(3,1,figsize=(10,10), squeeze=False)
        cl = None
        for i,j in itertools.product(*[range(x) for x in axs.shape]):
            if np.array(list(observables.keys())).reshape(axs.shape)[i,j] == 'Zeff':
                cl = [0,3]
            
            plot_reg(y = y, inds = idxnnan&inds, params = nlls_obj, title = "%s"%tuple([x['shot'].unique()[ishot]]),
                     label='$f = %.2f P_{ECRH}^{%.2f}\\bar{n}_e^{%.2f}Z_{eff}^{%.2f}$'%(tuple(nlls_obj.x)), juice = None, str_qty = 'Prad', 
                     scat_col = np.array(list(observables.keys())).reshape(axs.shape)[i,j], fig=fig, ax= axs[i,j],**observables)
    interact(f_interact, ishot=ishot)
import importlib
import project_autumn_scripts
importlib.reload(project_autumn_scripts)
from project_autumn_scripts import juice_utils
from project_autumn_scripts.juice_utils import power_analysis
def plot_model_vs_exp(x, shot, badShots, cis_key, png_port = 'AEI30', init_regpars = [1.,1., 9.,1.95, 1.,1.], plotovd = False, bconfig = 'EJM'):
    # get exp data from ovd, prad = ovd.PRAD
    #get regression data
    Pradh, Pradv,Pheat, fradh, fradv,  ne, zeff, png, inds = power_analysis(x = x, shotlist = shot, bconfig = bconfig, p_neu_gauge = True, key_png = 'p_neutral_gauge_port_'+png_port)
    # update indices exluding bad shots for CIS data and for specific shot
    inds_update = inds & (~x['shot'].isin(badShots))
    inds_shot = inds_update & (x['shot'].isin([shot]))
    # initial params and regression data
    qtis_cis_shot = {'pheat':Pheat, 'nbar_e':ne, 'Zeff':zeff, 'png':png, 'cis':x[cis_key][inds_update]}
    # compute params
    nlls_prad, idxnnan_prad = nlls_gen(func_regr = func_regr, x0 = init_regpars, y=Pradh, **qtis_cis_shot)
    # compute model data with params
    prad_model = func_regr(beta=nlls_prad.x, **qtis_cis_shot)
    # plot exp vs model
    plt.figure(figsize=(10,7))
    plt.title(shot + " $P_{rad}$ model vs exp data ")
    plt.plot(x['t_in_shot'][inds_shot], prad_model[inds_shot]*1e6, 'og', label='$P_{rad, model}$')
    plt.plot(x['t_in_shot'][inds_shot], Pradh[inds_shot]*1e6, '-C3', label='$P_{rad,exp, Juice}$')
    if plotovd:
        plt.plot(ovd.PRAD['t'], ovd.PRAD['s'], '-C1', label='$P_{rad,exp w OVD}$')

    inds_res = inds_shot[inds][idxnnan_prad]
    plt.fill_between(x['t_in_shot'][inds_shot], (prad_model[inds_shot] - (nlls_prad.fun[inds_res]**2)**0.5)*1e6, 
                 (prad_model[inds_shot] + (nlls_prad.fun[inds_res]**2)**0.5)*1e6, label='$P_{rad, residual}$')
    plt.xlabel("$t[\mathrm{s}]$")
    plt.ylabel("$P_{rad}[\mathrm{MW}]$")
    plt.xlim(min(x['t_in_shot'][inds_shot])-1, max(x['t_in_shot'][inds_shot])+1)
    plt.legend()
    
def pressure_trace(port_div, port_mc, x, fradh, fn = None, savefig = False):
    for shot in shotlist:
        ind_shot = x['shot'].isin([shot]) & inds
        fig, axs = plt.subplots(1,2)
        fig.suptitle("Time evolution of $p_{neutral,gauge}$ for shot "+shot)
        fig.set_size_inches(20,5)
        ax1 = axs[0].scatter(fradh[ind_shot[inds]],x['p_neutral_gauge_port_'+port_div][ind_shot], 
                             c = x['t_in_shot'][ind_shot], cmap='jet', label="$p_{div, %s}$"%port_div)
        cbar1 = fig.colorbar(ax1, ax = axs[0])
        cbar1.ax.set_xlabel("$t$")
        ax2 = axs[1].scatter(fradh[ind_shot[inds]],x['p_neutral_gauge_port_'+port_mc][ind_shot], 
                             c = x['t_in_shot'][ind_shot], cmap='jet', label="$p_{MC, %s}$"%port_mc)
        cbar2= fig.colorbar(ax2, ax = axs[1])
        cbar2.ax.set_xlabel("$t$")
        for ax in axs:
            ax.set_ylabel("$p$")
            ax.set_xlabel("$f_{rad}$")
        fig.tight_layout()
        axs[0].legend(loc='best')
        axs[1].legend(loc='best')
        if savefig:
            plt.savefig(cwd + "/Plots/time_evo_pneut_"+shot + ".png")
def plot_indexing(x, inds, qtis,port='AEI30', plot_folder = None,  fn = None, title = ": NLLS for data at detachment: $f_{rad}\geq 0.8$"):
    init_params = np.ones(len(qtis)+1)
    # plot Prad for detached
    nlls_prad, idxnnan_prad = nlls_gen(func_regr = func_regr, x0 = init_params, y=x['prad_hbc_MW'], **qtis)
    plot_reg(y = x['prad_hbc_MW'], inds = idxnnan_prad&inds, params = nlls_prad,
             title = port + title,
             label='$f = %.2f P_{ECRH}^{%.2f}\\bar{n}_e^{%.2f} Z_{eff}^{%.2f}p_{neutral,gauge}^{%.2f}I_{C^{2+}}^{%.2f}$'%(tuple(nlls_prad.x)), juice = None, str_qty = 'Prad',
             scat_col = 'cis', plot_folder=plot_folder,**qtis)
    if fn & plot_folder:
        plt.savefig(plot_folder +port+ fn)
    fradh_tot = x['ecrh_total_MW']+x['nbi_total_MW'].apply(lambda x: 0 if np.isnan(x) else x)
    nlls_frad, idxnnan_frad = nlls_gen(func_regr = func_regr, x0 = init_params, y=fradh_tot, **qtis)
    plot_reg(y = fradh_tot, inds = idxnnan_frad&inds, params = nlls_frad,
             title = port + title ,
             label='$f = %.2f P_{ECRH}^{%.2f}\\bar{n}_e^{%.2f} Z_{eff}^{%.2f}p_{neutral,gauge}^{%.2f}I_{C^{2+}}^{%.2f}$'%(tuple(nlls_frad.x)), juice = None, str_qty = 'frad',
             scat_col = 'cis', plot_folder=plot_folder,**qtis)
    if fn & plot_folder:
        plt.savefig(plot_folder+port + fn)
    
import plotly.express as px            
def plot_reg_plotly(y, inds, params, title, label, juice, scat_col, berr = False, str_qty = 'frad',savefig = True, fig = None, ax = None, xlim = None, ylim = None, collim = None,**kwargs):
    y_model = func_regr(beta=params.x, **kwargs)
    print(y.shape)
    df = pd.DataFrame()
    df['x'] = y
    df['y'] = y_model
    juice[inds][scat_col].head()
    df['col'] = juice[inds][scat_col]
    print(df)
    fig = px.scatter(
    df, x='x', y='y', opacity=0.65,
    trendline='ols', trendline_color_override='darkblue'
    )
    fig.show()
    
def colin_ports(x, labdict, inds, label_signalpath, shotlist, cis_key, col_key = 'cis', savefig =False, plot_folder = '/home/IPP-HGW/joag/Project_Autumn/Plots/', incl_pfrad = None, add_title = None, bconfig = 'EJM'):
    for port in list(label_signalpath.keys()):
        Pradh, Pradv,Pheat, fradh, fradv,  ne, zeff, png, inds = power_analysis(x = x, shotlist = shotlist, bconfig = bconfig, p_neu_gauge = True, key_png = 'p_neutral_gauge_port_'+port)
        print(incl_pfrad) 
        if incl_pfrad == 'prad': 
            qtis = {'prad':Pradh,'pheat':Pheat[inds], 'nbar_e':ne[inds], 'Zeff':zeff[inds], 'png':png[inds], 'cis':x[cis_key][inds]}
        elif incl_pfrad == 'frad':
            qtis = {'frad':fradh,'pheat':Pheat[inds], 'nbar_e':ne[inds], 'Zeff':zeff[inds], 'png':png[inds], 'cis':x[cis_key][inds]}
        else:
            qtis = {'pheat':Pheat[inds], 'nbar_e':ne[inds], 'Zeff':zeff[inds], 'png':png[inds], 'cis':x[cis_key][inds]}
        fig, axs = plt.subplots(len(qtis), len(qtis))
        fig.suptitle('Pressure port ' + port + ' Collinearity of $\{$' + ('%s, '*len(labdict)%tuple([value for key,value in labdict.items()]))[:-2] + '$\}$')
        fig.set_size_inches(45,15)
        for i,j in itertools.product(*[range(x) for x in axs.shape]):
            key_i = list(qtis.keys())[i]
            key_j = list(qtis.keys())[j]
            if i == j:
                axs[i,j].hist(qtis[key_i], bins = 50)
            else: 
                scat = axs[i,j].scatter(qtis[key_i],qtis[key_j], c = qtis[col_key], cmap='jet')
            axs[i,j].set_xlabel(str(labdict[key_i]))
            axs[i,j].set_ylabel(str(labdict[key_j]))
        fig.tight_layout()
        cbar = fig.colorbar(scat, ax = axs.ravel().tolist())
        cbar.ax.set_xlabel(labdict[col_key])
        if savefig:
            if add_title:
                plt.savefig(plot_folder + '/collinearity/' + add_title + port + '_collinearity_plot_col_'+col_key)
            else:
                plt.savefig(plot_folder + '/collinearity/' + port + '_collinearity_plot_col_'+col_key)
