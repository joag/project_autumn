import requests
import urllib
import w7xarchive

"""
## Neutral Pressure gauge data to juice dataformat
1. First cutoff the lowest and maximum time according to juice database to eliminate unuseful data.
2. Average over each shot for every 200 ms.
3. For each discharge store the averaged data for each time at b for average interval [a,b]
4. Plot data and confirm that there is overlap between discharge label, time at b for avg interval [a,b]
5. Make a master function that import from archive do the averages and adds to the database in the right index.
""""

# 200 ms averaging
def avg_w7xarchive(time, signal):
    """ 
    Args:
     - time - array of times of the discharge
     - signal - array of values of the measured quantity
    Out:
     - the averaged time and signal, time interval 200ms
    """
    t0 = min(time)
    #print(t0)
    t_avg = []
    signal_avg = []
    t = t0
    #print((max(times[0]) - min(times[0]))//0.2)
    for dt in range(int((max(time) - min(time))//0.2) ):
        t += 0.2
        if t > max(time):
            break
        #print("b at avg interval is", t)
        ind_b = np.where(np.isclose(time - t, 0, atol=1e-7) == True)[0] # index at b for avg interval [a,b]
        #print(ind_b)
        if len(ind_b) < 1:
            for i in range(6):
                ind_b = np.where(np.isclose(time - t, 0, atol=1e-1*(i+1)) == True)[0] # index at b for avg interval [a,b]
                if len(ind_b) >= 1:
                    break
        if len(ind_b) > 1:
            ind_b = ind_b[0]
        ind_b = int(ind_b)

        t_avg.append(t)
        signal_avg.append(signal[ind_b])
    return t_avg, signal_avg
# add new column to juice
def add_avg_w7xarchive(label_signalpath, shotlist, juice_pd, newkey, w7x_arch_lab = 'port'):
    """
    adding avg (over every 200ms) data of the qty specified in label_signalpath
    Args: 
    - label_signalpath - dict, the label and adress needed to read the data from the w7xarvchive
    - shotlist - list, discharges that the user wants to add to the juice database for the quantitie(s) speicified in the label_singalpath
    - panda of the juice database
    Out: no return, just added to the juice database
    """
    
    for key, value in label_signalpath.items():
        # get the signal and time from the database
        # making a column for each port
        juice_pd[newkey +'_%s_'%w7x_arch_lab+ key] = np.empty
        juice_pd[newkey +'_%s_'%w7x_arch_lab+ key] = np.nan
        print("New colmn name", newkey +'_%s_'%w7x_arch_lab+ key)
        for program in shotlist:
            try:
                time,signal=w7xarchive.get_signal_for_program(value, program[3:])
            except urllib.error.HTTPError as exception:
                print(exception)
            tavg, savg = avg_w7xarchive(time=time, signal=signal)
            # add signal value to right index in the right column
            for i in range(len(tavg)):
                inds = (juice_pd['shot'] == program) & (np.isclose(juice_pd['t_in_shot'], tavg[i], atol=1e-1))
                juice_pd[newkey +'_%s_'%w7x_arch_lab+ key][inds] = savg[i]
    return juice_pd

