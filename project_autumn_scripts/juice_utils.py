import numpy as np
import pandas as pd
def power_analysis(x, bconfig, shotlist, current_control = False, cc_val = 1, curr = None, shots=None, p_neu_gauge = False, key_png = None, constr_valeria = False):
    """
    Treatment of the data of interest from the database. Conditions set on the data.
    Args:
    Out:
     - observables of interest that needs restrictive conditions imported form the Juice database
    """
    # diff between inds and inds2 is that inds is the indices for all shots in the database
    # and inds2 is the shots specified in the shotlist in the second cellt
    inds = ([config[:3] == bconfig for config in x.configuration] &
            (x["t_in_shot"] > 0.2) & (x["t_shot_stop"]-x["t_in_shot"] > 0.1) & # skip start-up and shut-down
            ((x["shot_with_pellets"] == 0) | (x["t_in_shot"] < x["t_pellet_start"]-0.1)) &
            ((x["shot_with_tespel"] == 0) | (x["t_in_shot"] < x["t_tespel"]-0.1))&
            ((x["shot_with_lbo"] == 0) | (x["t_in_shot"] < x["t_lbo_1"]-0.1))&
            ((x["shot_with_nbi"]==0) | (x["t_in_shot"] < x["t_nbi_start"]-0.1))&
            (x["ecrh_total_MW"] > 0.25) & # ecrh data available
            (x["neL_1e19_m2"] > 0.25) & # density data available
            # stationarity, low oscillations
            (np.abs(x["dneL_dt_1e19_m2_s"]/x["neL_1e19_m2"]) < 1.) & # density ramp not too steep
            (np.abs(x["dwdia_dt_MJ_s"]/x["wdia_MJ"]) < 1.) & # plasma stored energy ramp not too steep
            (np.abs(x["max_wdia_MJ"] - x["min_wdia_MJ"])/x["wdia_MJ"] < 1.) & # diff min max plasma stored energy < 1
            (np.abs(x["max_neL_1e19_m2"] - x["min_neL_1e19_m2"])/x["neL_1e19_m2"] < 1.) &
            ((x["h_to_he"].isna()) | (x["h_to_he"] > .8)) & # mainly hydrogen
            ((x["prad_vbc_MW"].isna()) | (x["prad_vbc_MW"]/x["ecrh_total_MW"] < 2)) & # not radiatively collapsing
            ((x["prad_vbc_MW"].isna()) | (np.abs(x["max_prad_vbc_MW"]-x["min_prad_vbc_MW"])/x["prad_vbc_MW"] < 1))&
            ((x["prad_hbc_MW"].isna()) | (x["prad_hbc_MW"]/x["ecrh_total_MW"] < 2)) &
            ((x["prad_hbc_MW"].isna()) | (np.abs(x["max_prad_hbc_MW"]-x["min_prad_hbc_MW"])/x["prad_hbc_MW"] < 1))&
            ((x["prad_hbc_MW"].isna()) | (x["prad_hbc_MW"] >0))#&
            #((x["prad_hbc_MW"].isna() & x["prad_vbc_MW"].isna() ) |(np.abs(x["max_prad_hbc_MW"]-x["min_prad_vbc_MW"])/x["prad_hbc_MW"] < 3.6)) 
            )
      
    # valerias constraints
    endcampaignDay = np.array([i.find("W7X20181018") >= 0 for i in x['shot']])
    laserInstallTime = 1534232894037840521 #first shot with CIS laser: 20180814.006
    isAfter_laser = x.index > laserInstallTime

    badShots = [
                'W7X20180927.036', 'W7X20180927.037', 'W7X20180927.038', 'W7X20180927.039',
                'W7X20180927.040', 'W7X20180927.041', 'W7X20180927.042', 'W7X20180927.043',
                'W7X20180927.044', 'W7X20180927.045', 'W7X20180927.046', 'W7X20180821.021', #shots with He filter
                'W7X20180823.037', #Hbeta filter in
                'W7X20180823.015', #saturated
                'W7X20180905.004', 'W7X20180920.021', 'W7X20180920.022', 'W7X20180925.011',
                'W7X20180925.021', 'W7X20180925.022', 'W7X20180925.023', 'W7X20180927.035',
                'W7X20180816.019',
                'W7X20181009.011', 'W7X20181009.042', 'W7X20181011.009', #no calibration
                'W7X20181009.030', 'W7X20181009.034', 'W7X20181009.039', 'W7X20181010.025',
                'W7X20181011.004', 'W7X20181011.007', 'W7X20181011.011', 'W7X20181011.035',
                #calibration img10 off from correct wavelength
                'W7X20180911.030', 'W7X20180919.038', #laser on during plasma
                'W7X20181011.025', #pulse train
                'W7X20181011.027', 'W7X20181011.036', #no filter
                'W7X20181011.024', 'W7X20181011.026', 'W7X20181011.028', 'W7X20181011.029',
                'W7X20181011.033', 'W7X20181011.034', #N2 puff from He beam module 3 --> line in CIII filter
                'W7X20180816.022', # wrong Wdia in archive
                'W7X20181004.048', # broken ne after 7s
                'W7X20180823.010', 'W7X20180822.017', 'W7X20180829.022', # total power is wrong
                ]
    isGood = (~x["shot"].isin(badShots))

    impurity_seeding_Ne = [
                        'W7X20180905.019', 'W7X20180905.020', 'W7X20180905.021',
                        'W7X20180905.022',
                        'W7X20180920.040', 'W7X20180920.041', 'W7X20180920.042',
                        'W7X20180920.043', 'W7X20180920.044','W7X20180920.045',
                        'W7X20180920.046',
                        ]
    wImpurity_Seeding_Ne = np.array([i in impurity_seeding_Ne for i in list(x["shot"])])
    impurity_seeding_N2 = [
                        'W7X20180905.024', 'W7X20180905.026',
                        'W7X20180905.027', 'W7X20180905.028',
                        'W7X20180920.047', 'W7X20180920.048', 'W7X20180920.049',
                        'W7X20180920.050', 'W7X20180920.051',
                        'W7X20181016.022', 'W7X20181016.023',
                        ]
    wImpurity_Seeding_N2 = np.array([i in impurity_seeding_N2 for i in list(x["shot"])])

    cfg = "EJM"
    isCfg = np.array([i.startswith(cfg) for i in list(x["configuration"])])   
    he_shots = np.array([i<0.8 for i in list(x["h_to_he_corrected"])])

    flt = ((x['ecrh_total_MW'] > 0.3) # ECRH data available and sensible
            & (x["neL_1e19_m2"] > 0.25) # density data available
            & isGood
            & isAfter_laser
            & isCfg # defined magnetic configuration
            & ((x["shot_with_nbi"]==0) | (x["t_in_shot"] < x["t_nbi_start"]-0.1) | (x["nbi_total_MW"] < 0.7)) # allow small blips
            & ~endcampaignDay # no last day of the campaign
            & ~he_shots # no He shots
            & ~wImpurity_Seeding_Ne
            & ~wImpurity_Seeding_N2
            & ((x["shot_with_tespel"] == 0) | (x["t_in_shot"] < x["t_tespel"]-0.1) | (x["t_in_shot"] > x["t_tespel"]+2.0)) #no tespel injection
            & ((x["shot_with_lbo"] == 0) | (x["t_in_shot"] < x["t_lbo_1"]-0.1) | (x["t_in_shot"] > x["t_lbo_1"]+2.0)) #no lbo
            & (x["shot_with_main_he_injection"] == 0) # presumably no He plasmas
            & (x["shot_with_main_ne_injection"] == 0) # no seeding through the main valves
            & (x["shot_with_main_n2_injection"] == 0)
            & np.abs(x["dneL_dt_1e19_m2_s"])<3
            & (x["t_in_shot"] > 0.1) & (x["t_shot_stop"]-x["t_in_shot"] > 0.1)
            & ((x["prad_hbc_MW"].isna()) | (x["prad_hbc_MW"]/x["ecrh_total_MW"] < 2)) &
            ((x["prad_hbc_MW"].isna()) | (np.abs(x["max_prad_hbc_MW"]-x["min_prad_hbc_MW"])/x["prad_hbc_MW"] < 1))
            & (x['prad_hbc_MW']>=0.0)
            & ((x["shot_with_pellets"] == 0) | (x["t_in_shot"] < x["t_pellet_start"]-0.1) | (x["t_in_shot"] > x["t_pellet_stop"]+2.0)) # no pellet injections
            )
    
    if constr_valeria:
        inds = flt
    
    if shots:
        inds = inds & x["shot"].isin(shots)
    
    if current_control:
        print("Len inds before cc:", len(inds))
        inds_0 = inds & (0.01 > np.abs(x["coil_control_" + str(cc_val) + "_kA"]))
        inds_001_1 = inds & (0.01 <= np.abs(x["coil_control_" + str(cc_val) + "_kA"])) & (np.abs(x["coil_control_" + str(cc_val) + "_kA"]) <= 1)
        inds_above1 = inds & (np.abs(x["coil_control_" + str(cc_val) + "_kA"]) > 1)

    if curr:
        inds = locals()['inds_' + str(curr)]
    
    Pradh=x['prad_hbc_MW'][inds]

    Pradv=x['prad_vbc_MW'][inds]

    Pheat=x['ecrh_total_MW'][inds]+x['nbi_total_MW'][inds].apply(lambda x: 0 if np.isnan(x) else x)

    fradh=np.divide(Pradh,Pheat)
    fradv=np.divide(Pradv,Pheat)

    ne=x['neL_1e19_m2'][inds]
    zeff=x['zeff'][inds]
    zeff_main_imp_dis = x['zeff_main_impurity_charge'][inds]
    zeff_version = x['zeff_version'][inds]
    if p_neu_gauge:
        png = x[key_png][inds]
        return Pradh, Pradv, Pheat, fradh, fradv, ne, zeff, png, inds
    else:
        return Pradh, Pradv, Pheat, fradh, fradv, ne, zeff, inds


def frad_cor(fradh, fradv):
    fradhcor=np.divide(fradh,(1+0.1*fradh))

    fradvcor=np.divide(fradv,(1+0.3*fradv))

    return fradhcor, fradvcor