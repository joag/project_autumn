from scipy.optimize import least_squares
import itertools
import numpy as np

def log_func_regr(beta, **kwargs):
    """
    General function to to use in regression. 
    Can add as many parameter and statstical variables you want.
    """
    
    if len(beta[1:]) != len(kwargs):
        raise ValueError('The number of parameters does not match the number of quantities')
    ret = beta[0]
    i = 1

    
    # make keys env variables eq to valu    #if len(kwargs) ==1:
    #    kwargs = kwargs['kwargs']
    for key,value in kwargs.items():
        ret += beta[i]*np.log(value)
        i += 1
    return ret


def func_regr(beta, **kwargs):
    """
    General function to to use in regression. 
    Can add as many parameter and statstical variables you want.
    """
    
    if len(beta[1:]) != len(kwargs):
        raise ValueError('The number of parameters does not match the number of quantities')
    ret = beta[0]
    i = 1

    
    # make keys env variables eq to valu    #if len(kwargs) ==1:
    #    kwargs = kwargs['kwargs']
    for key,value in kwargs.items():
        ret *= value**beta[i]
        i += 1
    return ret

def func_regr_linear(beta, **kwargs):
    """
    3rd order expansion of expression in func_regr_secondorder 
    Can add as many parameter and statstical variables you want.
    """
    
    if len(beta[1:]) != len(kwargs):
        raise ValueError('The number of parameters does not match the number of quantities')
    ret = beta[0]
    i = 1

    
    # make keys env variables eq to valu    #if len(kwargs) ==1:
    #    kwargs = kwargs['kwargs']
    for key,value in kwargs.items():
        ret += value*beta[i]
        i += 1
    return ret

def func_regr_3rd_ord_exp(beta, **kwargs):
    """
    3rd order expansion of expression in func_regr_secondorder 
    Can add as many parameter and statstical variables you want.
    """
    
    if len(beta[1:]) != len(kwargs):
        raise ValueError('The number of parameters does not match the number of quantities')
    ret = 1
    i = 1

    
    # make keys env variables eq to valu    #if len(kwargs) ==1:
    #    kwargs = kwargs['kwargs']
    for key,value in kwargs.items():
        ret *= value**(.5*beta[i])
        i += 1
    return .5*beta[0]*(ret**2 + ret**4/4 + ret**6/8)

def func_regr_secondorder(beta, **kwargs):
    """
    Specific function to to use in regression for the second order equation derived in the paper. 
    Can add as many parameter and statstical variables you want to the RHS as defined in the project report.
    RHS = integral over transport and radiation loss: int_Td^Tu f(n,T)*f_Z*L_Z dT
    
    Assuming that the SOL widht \lambda_SOL is 2cm so Pheat/\lambda_SOL = q_u ; \lambda_SOL = 2cm
    """
    if 'pheat' in list(kwargs.keys()):
        if len(beta[1:]) != len(kwargs):
            raise ValueError('The number of parameters does not match the number of quantities')
        ret = 1
        #ret = beta[0]
        
        i = 1


        # make keys env variables eq to valu    #if len(kwargs) ==1:
        #    kwargs = kwargs['kwargs']
        for key,value in kwargs.items():
            ret *= value**beta[i]
            i += 1
        
        #ind = ret.notna()
        #print(ret[ind])
        return 1-np.sqrt(1-ret)
        #return np.sqrt(1-np.sqrt(1-ret))
    else:
        print("Pheat must be included in this regression")
        return None

rhos = ['linear', 'soft_l1', 'huber', 'cauchy', 'arctan']

def nlls_gen(func_regr, x0, y, bounds = (-np.inf, np.inf), rho = rhos[0], fscale = 1, **kwargs):
    """
    General non linear least square routine using scipy and arb number of params and statstical quantities
    Args:
    - func_regr - regression model function
    - x0 - initial conditions
    - y - data on
    - bounds - parameter bounds
    - rho - loss function (affects the impact of outliers), string or function that takes in 1D array
    Out:
    - optimized regression parameters, indices for data containing no NaN vals
    """
    idx_nnan = []
    # Non-linear LS (NLLS)
    def f(beta, **kwargs):
        nonlocal idx_nnan
    
        fval= func_regr(beta=beta,**kwargs)
        res = fval - y
        #print("len residuals", len(res))
        # for storing the non-NaN values
        idx_nnan = [res.notna()][0]
        return res[idx_nnan]

    spopt_res = least_squares(fun = f, x0 = x0, bounds = bounds, loss = rho, f_scale = fscale,kwargs=kwargs)#bounds=([1,0,1],[20,1,3])
    return spopt_res, idx_nnan