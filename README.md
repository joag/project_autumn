# project_autumn
Semester project for Max Planck institute autumn 2021. The project is regarding scaling the radiated power using global plasma parameters.

## cross_field_dominates.ipynb  

A notebook decribing the heat transport if cross field transport is dominant which is most likely the case for stellarators such as w7x. The cross-field transport is compared to the parallel transport often seen in poloidal divertors in tokamaks.

## Field_line_power_balance.ipynb  

Arbitrary parmaeters from a EMC3-EIRENE scalar field can be traced along field lines. The purpose is to develop a tool that can calculate power balance along field lines.

## Juice_notebook_without_Z_inreg.ipynb  

The main notebook for exploring the Juice database with respect to a scaling for the radiated power. Main focus is a regression model for the radiated power and the radiated power fraction using data from main plasma parameters.

### Requirements for running Juice_notebook_without_Z_inreg.ipynb
- Juice data base Link: https://datashare.mpcdf.mpg.de/s/7lIBZER60hWZQvl
- W7X data aquisition and plotting tools developed by Felix Reimold: 
  - w7xarchive: the archive for acquiring data from every operational diagnostic at IPP
  - w7x-overviewplot: A collection of python scripts to automatically generate overview plots for the Wendelstein 7-X stellarator.
  - utilities: python utility and helper functions related to postprocessing of diagnostic data


## pressure_gauges_archive_test.ipynb  

This is a test notebook for reading data and postprocessing data for adding it to the juice database using the neutral pressure as an example measurable.

## radiated_cooling_rate.ipynb

This is a notebook showing som examples from the cherab library on how to calculate the fractional abundances and photon emission coefficients (PEC) of an impurity.

## project_autumn_scripts

Module for all the tools used on the Juice database. It includes the an implementation of a non-linear least square (NLLS) model for generalized functions and arbitrary number of observables using the scipy implementation of NLLS. In the implementation for observables <img src="https://render.githubusercontent.com/render/math?math=$\{ O_i \}_{i=1}^N$">  the regression function is defined as the product of each observalbe to the power of some regression parameter <img src="https://render.githubusercontent.com/render/math?math=$\beta_i$"> as given in the equation below:

<img src="https://render.githubusercontent.com/render/math?math=$f(\{ O_i \}_{i=1}^N) = \alpha \Pi_{i=1}^N O_i^{\beta_i}$">

where <img src="https://render.githubusercontent.com/render/math?math=$\alpha$"> and <img src="https://render.githubusercontent.com/render/math?math=$\beta_i$"> represents the regression parameter to optimize to best fit the target data.

Otherwise the module consists of a collection of plotting tools and dataprocessing routines. They should be described with docstrings.
